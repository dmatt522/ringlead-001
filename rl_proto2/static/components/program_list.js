define([
    'vue',
    'moment'

], function(Vue, moment){
    return Vue.extend({

        replace: false,

        data: function() {
            return {
                programItems: [],
                fetching: false,
                nameFilter: '',
                statusFilter: 'active'
            }
        },

        watch: {
            'nameFilter': function (val, oldVal) {
                this.fetchPrograms();
            },
            'statusFilter': function (val, oldVal) {
                this.fetchPrograms();
            }
        },

        events: {
            eventReceived: function(event) {
                if ( ! event.program_changed)
                    return;

                var self = this, consumeEvents = [];

                event.program_changed.forEach(function(e){
                    var item = e.event_data.data;

                    var index = self.programItems.map(function(i) { return i.id; }).indexOf(item.id);

                    if (index > -1) {
                        self.programItems.splice(index, 1, item);
                    }
                    else {
                        self.programItems.push(item);
                    }

                    consumeEvents.push(e.event_id);
                });

                this.$dispatch('markConsumed', consumeEvents);
            }
        },

        ready: function() {
            this.fetchPrograms();
        },

        computed: {
            visibleProgramsCount: function() {
                var self = this, count = 0;

                this.programItems.forEach(function(item){
                    if (self.isVisible(item)) {
                        count += 1;
                    }
                });

                return count;
            }
        },

        methods: {
            fetchPrograms: function() {
                var self = this;

                self.$set('fetching', true);

                $.ajax({
                    url: '/api/program/',
                    method: 'GET',
                    data: {
                        nameFilter: self.nameFilter,
                        statusFilter: self.statusFilter
                    },
                    success: function(data, text_status, jqXHR) {
                        self.$set('fetching', false);
                        self.$set('programItems', data);
                    }
                });
            },

            isVisible: function(item) {
                return (item.status == this.statusFilter) || (item.status != 'ARCH' && this.statusFilter == 'active');
            },

            showEdit: function(item) {
                return /COMPL|CANCEL/.test(item.status);
            },

            showCancel: function(item) {
                return /RUN|ACT|PEND/.test(item.status);
            },

            showRestart: function(item) {
                return /PAUSE|ERROR/.test(item.status);
            },

            showArchive: function(item) {
                return /ERROR|COMPL|CANCEL/.test(item.status);
            },

            getFormattedDate: function(dateString) {
                return moment(dateString).format('MMM Do YYYY, h:mm a');
            },

            getStatusIcon: function(item) {
                var icons = {
                    'ACT': 'fa-bolt',
                    'CREA': 'fa-file-o',
                    'RUN': 'fa-circle-o-notch fa-spin',
                    'PEND': 'fa-circle-o-notch fa-spin',
                    'PAUSE': 'fa-pause',
                    'ERROR': 'fa-exclamation-triangle',
                    'COMPL': 'fa-check',
                    'CANCEL': 'fa-times-circle',
                    'ARCH': 'fa-archive'
                };

                return icons[item.status];
            },

            getLabelType: function(item) {
                var labels = {
                    'ACT': 'warning',
                    'CREA': 'primary',
                    'RUN': 'primary',
                    'PEND': 'primary',
                    'PAUSE': 'default',
                    'ERROR': 'danger',
                    'COMPL': 'success',
                    'CANCEL': 'danger',
                    'ARCH': 'default'
                };

                return labels[item.status];
            },

            processProgramAction: function(url) {
                var self = this;

                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function(data, text_status, jqXHR) {
                        self.fetchPrograms();
                    }
                });
            },

            isEmptyObject: function(obj) {
                return $.isEmptyObject(obj);
            }
        }

    });
});
