define([
    'vue'

], function(Vue){
    return Vue.extend({

        data: function() {
            return {
                showModal: false,

                currentNotification: {
                    subject: '',
                    text: '',
                    event: {
                        id: 0
                    }
                }
            }
        },

        computed: {
            modalClass: function() {
                return this.showModal ? 'in' : '';
            },

            modalDisplay: function() {
                return this.showModal ? 'block' : 'none';
            }
        },

        methods: {
            showNotification: function() {
                // show modal
                this.showModal = true;

                // mark consumed
                if (this.currentNotification.event.id > 0) {
                    this.$dispatch(
                        'markConsumed', [this.currentNotification.event.id]);
                }
            }
        }

    });
});
