define([
    'vue'

], function(Vue) {
    return Vue.extend({
        template: '#list-import-dropzone',
        props: ['passed'],

        ready: function() {
            var self = this,
                container = $(this.$el),
                input = container.find(':input'),
                dropzone = container.find('#dropzone');

            dropzone.click(function(){
                input.click();
            });

            input.fileupload({
                dropZone: dropzone,
                dataType: 'json',
                forceIframeTransport: true,
                done: function(e, data) {
                    var response = data.response().result;
                    window.location.replace(response.redirect_url);
                }
            });
        }
    });
});
