define([
    '/static/components/notification.js'

], function(Notification){
    return Notification.extend({

        replace: false,

        ready: function() {
            $(this.$el).show();
        },

        data: function() {
            return {
                notifications: [],
                shownNotifications: {}
            }
        },

        computed: {
            unread: function() {
                var total = this.notifications.length;

                // shownNotifications is an auxiliary object that holds
                // read notifications, so we can have a real-time count
                // before the server consumes the mark-read event:
                //
                // (total notifications - shown notifications)
                for (var key in this.shownNotifications) {
                    if (this.shownNotifications.hasOwnProperty(key)) {
                        total -= 1;
                    }
                }

                return total;
            }
        },

        events: {
            eventReceived: function(event) {
                var notifications = [];

                if (event.notification) {
                    event.notification.forEach(function(e){
                        notifications.push({
                            subject: e.event_data.data.subject,
                            text: e.event_data.data.text,
                            event: {
                                id: e.event_id
                            }
                        });
                    });
                }

                this.notifications = notifications;
            }
        },

        methods: {
            showNotificationByIndex: function(index) {
                this.currentNotification = this.notifications[index];
                this.shownNotifications[this.currentNotification.event.id] = true;
                this.showNotification();
            }
        }

    });
});
