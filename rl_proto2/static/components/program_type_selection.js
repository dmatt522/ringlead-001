define([
    'vue'

], function(Vue) {
    return Vue.extend({
        template: '#program-type-selection',
        props: ['passed'],

        data: function() {
            return {
                downloadJobStarted: null,
                downloadDbJobPk: null,
                newProgramPk: null
            }
        },

        methods: {
            checkDownloadDbJob: function() {
                var self = this;
                job_status(self.downloadDbJobPk, function(status) {
                  if (status['status'] == 'SUCCESS') {
                    var programBuilderUrl = '/batch_select/?pk=' + self.newProgramPk;
                    self.downloadDbJobPk = null;
                    self.newProgramPk = null;
                    window.location.replace(programBuilderUrl);
                  } else if (status['status'] == 'FAILURE') {
                    window.location.replace('/program/schema_download_failure/' + self.downloadDbJobPk);
                  }
                });
                setTimeout(self.checkDownloadDbJob, 1000);
            },
            onDbClick: function(e) {
                var self = this;

                $('#new-program-modal-body-row').hide();
                $('#new-program-modal-spinner').show();

                $.ajax({
                    url: '/download_database/',
                    method: 'GET',
                    success: function(data, text_status, jqXHR) {
                        self.downloadJobStarted = true;
                        self.downloadDbJobPk = data.job_pk;
                        self.newProgramPk = data.program_pk;
                        self.checkDownloadDbJob();
                    }
                });
            },
            onListImportClick: function(e) {
                this.$dispatch('view-change', 'list-import-dropzone');
            }
        },

        ready: function () {
            var self = this;

            $('#newProgramModal').on('hide.bs.modal', function (e) {
                if (self.downloadJobStarted) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    return;
                }
                $('#new-program-modal-body-row').show();
                $('#new-program-modal-spinner').hide();
            });
        }
    });
});
