# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import timezone_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0016_auto_20160217_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='schedule_timezone',
            field=timezone_field.fields.TimeZoneField(default=b'US/Eastern'),
        ),
    ]
